(function() {
  document.getElementById("app").addEventListener('input', function (e) {
    if(e.target.value) {
      document.getElementById("button_iniciar").disabled = false;
    } else {
      document.getElementById("button_iniciar").disabled = true;
    };
  });

  document.getElementById("app").value = "474129516357810";
  document.getElementById("button_iniciar").disabled = false;

  document.getElementById("button_iniciar").addEventListener("click", function(e){
    e.preventDefault();
    iniciarFbLogin(document.getElementById("app").value);
  });
}())

let page_sel = {};
let pages = [];
let form_sel = {};
let forms = [];
let leads = [];

function iniciarFbLogin(app_id) {
  window.fbAsyncInit = function() {
      FB.init({
          // appId      : '220974132334947',
          // appId      : '268168731225928',
          // appId      : '474129516357810',
          appId      : app_id,
          cookie     : true,
          xfbml      : true,
          version    : 'v7.0'
      });
          
      // FB.AppEvents.logPageView();

      FB.getLoginStatus(function(response) {
          statusChangeCallback(response);
      });
  };

  document.getElementById("form").style.display = "none";

  (function(d, s, id){
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {return;}
      js = d.createElement(s); js.id = id;
      js.src = "https://connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
}

// iniciarFbLogin("474129516357810");

function statusChangeCallback(response){
  if(response.status === 'connected'){
      setElements(true);
      // console.log('Logged in and authenticated');
      getPages();
  } else {
      // console.log('Not authenticated.');
      setElements(false);
  }
}

function checkLoginState() {
  FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
  });
}

function addPageToList(item, index) {
  if (document.getElementById(item.id)) return;
  let template = document.querySelector("#table-pages #item-row").innerHTML;
  template = template.replace("{{index}}", index);
  template = template.replace("{{name}}", item.name);
  template = template.replace("{{row-id}}", item.id);
  template = template.replace("{{item-id}}", item.id);
  document.getElementById("table-pages").tBodies[0].innerHTML += template;
}

function selectPage(id_item) {
  let page_selecc = pages.find((e) => e.id === id_item);
  page_sel = page_selecc || {};
  
  if(page_sel && page_sel.id) {
    getForms(page_sel);
  }
}

function addFormToList(item, index) {
  if (document.getElementById(item.id)) return;
  let template = document.querySelector("#table-forms #item-row").innerHTML;
  template = template.replace("{{index}}", index);
  template = template.replace("{{name}}", item.name);
  template = template.replace("{{status}}", item.status);
  template = template.replace("{{row-id}}", item.id);
  template = template.replace("{{item-id}}", item.id);
  document.getElementById("table-forms").tBodies[0].innerHTML += template;
}

function selectForm(id_item) {
  let form_selecc = forms.find((e) => e.id === id_item);
  form_sel = form_selecc || {};

  if(form_sel && form_sel.id) {
    getLeads(form_sel, page_sel);
  }
}

function addLeadToList(item, index) {
  if (document.getElementById(item.id)) return;
  let template = document.querySelector("#table-leads #item-row").innerHTML;
  template = template.replace("{{index}}", index);
  body = item.field_data.reduce((a, e) => {
    return a + `${e.name} : ${e.values.join(", ")} <br/>`;
  }, "");
  template = template.replace("{{body}}", body);
  template = template.replace("{{created_time}}", item.created_time);
  template = template.replace("{{row-id}}", item.id);
  template = template.replace("{{item-id}}", item.id);
  document.getElementById("table-leads").tBodies[0].innerHTML += template;
}

function getPages(){
  FB.api('me/accounts?fields=name,access_token', function(response){
      pages = response.data;
      let index = 0;
      pages.map((e)=>{
        index++;
        addPageToList(e, index);
      });
      
      // Al seleccionar la página, obtener y guardar el registro (objeto)
  })
}

function getForms(page){
  FB.api(`/${page.id}/leadgen_forms?access_token=${page.access_token}`, function(response){
    if(response && !response.error){
      forms = response.data;
      document.getElementById("table-pages").style.display = "none";
      document.getElementById("table-forms").style.display = "table";
      document.getElementById("table-title").innerText = "Formularios";
      let index = 0;
      forms.map((e)=>{
        index++;
        addFormToList(e, index);
      });

        // Al seleccionar el formulario, obtener y guardar el registro (objeto)
    }
  });
}

function getLeads(form, page) {
  FB.api(`/${form.id}/leads?access_token=${page.access_token}`, function(response){
    if(response && !response.error){
      leads = response.data;
      document.getElementById("table-forms").style.display = "none";
      document.getElementById("table-leads").style.display = "table";
      document.getElementById("table-title").innerText = "Leads";
      let index = 0;
      leads.map((e)=>{
        index++;
        addLeadToList(e, index);
      });
    }
  })
}

function setElements(isLoggedIn){
  if(isLoggedIn){
    document.getElementById('data').style.display = 'block';
    document.getElementById('logout').style.display = 'block';
    document.getElementById('fb-btn').style.display = 'none';
    document.getElementById('heading').style.display = 'none';
    document.getElementById('table-pages').style.display = 'table';
    document.getElementById('table-forms').style.display = 'none';
    document.getElementById('table-leads').style.display = 'none';
  } else {
    document.getElementById('data').style.display = 'none';
    document.getElementById('data').style.display = 'none';
    document.getElementById('logout').style.display = 'none';
    document.getElementById('fb-btn').style.display = 'block';
    document.getElementById('heading').style.display = 'block';
    document.getElementById('table-pages').style.display = 'none';
    document.getElementById('table-forms').style.display = 'none';
    document.getElementById('table-leads').style.display = 'none';
  }
}

function logout(){
  FB.logout(function(response){
      setElements(false);
  });
}